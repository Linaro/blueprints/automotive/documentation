# Enabling EWAOL on the AVA platform with r1

## Background

This documentation records the instructions for enabling EWAOL on the
AVA platform with releasing version r1.

## Prerequisites

Install dependencies on Ubuntu/Debian:
   ```console
   $ sudo -H pip3 install --upgrade kas==3.0.2
   $ sudo apt install bmap-tools
   $ sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib build-essential chrpath socat
   ```

This guide assumes you are performing the build on an Ubuntu 20.04 based
system.  If you are using other distros, please follow up the
instructions in section "Build EWAOL with Docker Container" for building
EWAOL image.

## EWAOL Build Instructions

### Build EWAOL with using kas

Checkout the necessary repositories and perform the build:
   ```
   mkdir -p ~/scdp
   cd ~/scdp
   git clone https://github.com/ADLINK/meta-adlink-ampere.git -b v1.4
   git clone https://git.gitlab.arm.com/ewaol/meta-ewaol.git -b v0.2.1
   kas build meta-adlink-ampere/ComHpc.yml
   ```

Note: Currently there have several modules are broken when use
kas, it suggests to build EWAOL r1 with "Vanilla" Yocto or with Docker
container, please see details in below sections.

### Build EWAOL with "Vanilla" Yocto

Clone the necessary repositories and add the newly clones Yocto layers
into your BitBake configuration: Note that you should be using `bash`
during this build process.
   ```console
   $ git clone https://git.yoctoproject.org/git/poky -b hardknott
   $ git clone https://github.com/openembedded/meta-openembedded.git -b hardknott
   $ git clone https://git.yoctoproject.org/git/meta-security -b hardknott
   $ git clone https://git.yoctoproject.org/git/meta-virtualization -b hardknott
   $ git clone https://git.yoctoproject.org/git/meta-arm -b hardknott
   $ git clone https://github.com/ADLINK/meta-adlink-ampere -b v1.4
   $ git clone https://gitlab.arm.com/ewaol/meta-ewaol -b v0.2.1

   $ source poky/oe-init-build-env
   $ bitbake-layers add-layer ../meta-openembedded/meta-oe
   $ bitbake-layers add-layer ../meta-openembedded/meta-filesystems
   $ bitbake-layers add-layer ../meta-openembedded/meta-perl
   $ bitbake-layers add-layer ../meta-openembedded/meta-python
   $ bitbake-layers add-layer ../meta-openembedded/meta-networking
   $ bitbake-layers add-layer ../meta-arm/meta-arm-toolchain
   $ bitbake-layers add-layer ../meta-arm/meta-arm
   $ bitbake-layers add-layer ../meta-security
   $ bitbake-layers add-layer ../meta-virtualization
   $ bitbake-layers add-layer ../meta-adlink-ampere
   $ bitbake-layers add-layer ../meta-ewaol/meta-ewaol-distro
   ```

Add the following lines to the local.conf file:
   ```
   $ echo -e 'MACHINE="comhpc"\n\
   DISTRO="ewaol"\n\
   ' >> build/conf/local.conf
   ```

Run the following command to initiate the build process:
   ```
   $ bitbake ewaol-image-docker
   ```

### Build EWAOL with Docker Container

It's possible that your installed system is not compatible for building
EWAOL r1.  In this case, you can consider to build EWAOL with Docker
container.

Create a `Dockerfile` with below content:

   ```
   FROM ubuntu:20.04

   RUN apt-get update
   ENV DEBIAN_FRONTEND=noninteractive
   RUN apt-get install -y gawk wget git diffstat unzip texinfo gcc build-essential \
           chrpath socat cpio python3 python3-pip python3-pexpect xz-utils debianutils \
           iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev pylint3 \
           xterm python3-subunit mesa-common-dev
   RUN apt-get install -y sudo vim locales
   RUN locale-gen en_US.UTF-8 && update-locale

   RUN useradd -m -r -s /bin/bash docker && adduser docker sudo && echo "docker:docker" | chpasswd

   USER docker

   RUN mkdir ~/hardknott
   WORKDIR /home/docker/hardknott

   RUN git clone https://git.yoctoproject.org/git/poky -b hardknott
   RUN git clone https://github.com/openembedded/meta-openembedded.git -b hardknott
   RUN git clone https://git.yoctoproject.org/git/meta-security -b hardknott
   RUN git clone https://git.yoctoproject.org/git/meta-virtualization -b hardknott
   RUN git clone https://git.yoctoproject.org/git/meta-arm -b hardknott
   RUN git clone https://github.com/ADLINK/meta-adlink-ampere -b v1.4
   RUN git clone https://gitlab.arm.com/ewaol/meta-ewaol -b v0.2

   SHELL ["/bin/bash", "-c"]
   RUN source poky/oe-init-build-env && \
           bitbake-layers add-layer ../meta-openembedded/meta-oe && \
           bitbake-layers add-layer ../meta-openembedded/meta-filesystems && \
           bitbake-layers add-layer ../meta-openembedded/meta-perl && \
           bitbake-layers add-layer ../meta-openembedded/meta-python && \
           bitbake-layers add-layer ../meta-openembedded/meta-networking && \
           bitbake-layers add-layer ../meta-arm/meta-arm-toolchain && \
           bitbake-layers add-layer ../meta-arm/meta-arm && \
           bitbake-layers add-layer ../meta-security && \
           bitbake-layers add-layer ../meta-virtualization && \
           bitbake-layers add-layer ../meta-adlink-ampere && \
           bitbake-layers add-layer ../meta-ewaol/meta-ewaol-distro

   RUN echo -e '\
   MACHINE="comhpc"\n\
   DISTRO="ewaol"\n\
   ' >> /home/docker/hardknott/build/conf/local.conf

   CMD source poky/oe-init-build-env && bitbake ewaol-image-docker
   ```

In the same directory as the `Dockerfile`, run below command to build
Docker image:
   ```
   $ docker build --tag soafee
   ```

Use Docker container to build the EWAOL image for the AVA Development
Platform:
   ```
   $ docker run -it --name soafee_builder soafee
   ```

In a separate terminal, while your Docker container is still running,
copy EWAOL image from Docker container:
   ```
   $ docker cp soafee_builder:/home/docker/hardknott/build/tmp/deploy/images/comhpc/ ./
   ```

## AVA Image Installation Instructions

Once the build process is complete, you will be able to find the
resultant image files in build/tmp/deploy/images/comhpc.

Copy the EWAOL images onto a USB stick
   ```console
   $ cd build/tmp/deploy/images/comhpc/
   $ sudo bmaptool copy --bmap ewaol-image-docker-comhpc.wic.bmap ewaol-image-docker-comhpc.wic.bz2 /dev/sdX
   ```

Alternatively, you may use the dd utility like so:
   ```console
   $ cd build/tmp/deploy/images/comhpc/
   $ sudo dd if=ewaol-image-docker-comhpc.rootfs.wic of=/dev/sdX bs=1M status=progress && sync
   ```

Note: Replace the `sdX` with the USB stick device name on your PC. You
can use `dmesg | tail -n 20` command to identify the desired USB stick device.

Once the writing is complete, safely remove and re-insert the USB
storage device to check if the partitions are readable and not
corrupted. Then remove the device safely once more and move over to the
AVA Development Platform.

## Boot EWAOL System

Insert the drive/USB stick into the machine, power the system on (hold
the power button for 2 seconds, then release) and wait for the
Tianocore/EDK2 firmware boot screen to appear. Press `<Escape>` to
interrupt the boot process. Under `Boot Manager`, select the UEFI
entry which corresponds to USB stick, and press `<Enter>`.

You should now be presented with GNU GRUB. Select
`USB Boot: COM-HPC Yocto Image` to boot system from USB stick.
